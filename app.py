from flask import Flask, request
import json
app = Flask(__name__)


@app.route("/")
def index():
    return "TEST"


@app.route("/version")
def version():
    return "0.1"


# Google App Engine will send an HTTP
# request to /_ah/heath and any 2xx or 404 response is considered healthy.
# Because 404 responses are considered healthy, this could actually be left
# out as nginx will return 404 if the file isn't found. However, it is better
# to be explicit.
@app.route("/_ah/health")
def health():
    return "ok"


# The start lifecycle event is sent to your application as soon as your container
# is built or provisioned. It is signaled as a HTTP request to the handler /_ah/start.
# No response is required to this handler, however your application might use it as
# a signal that App Engine expects your container to be ready to respond to incoming traffic.
@app.route("/_ah/start")
def start():
    return "started"


# The stop lifecycle event is sent to your application when App Engine is preparing
# to turn down the container. It is signaled as a HTTP request to the handler /_ah/stop.
# The application does not need to respond to this event - but can use it to perform
# any necessary clean up actions prior to the container being shut down.
@app.route("/_ah/stop")
def stop():
    return "stopped"


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
